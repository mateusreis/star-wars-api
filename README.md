Esse projeto foi criado com [Create React App](https://github.com/facebookincubator/create-react-app). 

Utiliza React, Sass e a api de [Star Wars](https://swapi.co/).

## Instalação

  1. Clone este repositório: **`git clone git@bitbucket.org:mateusreis/star-wars-api.git`**

  2. Execute **`npm i`**.

## Uso

### **`npm start`**

Inicia o ambiente de desenvolvimento e o preprocessor do Sass

### **`npm run build`**

Compila a aplicação no diretório `build`.


**Obs.: Executar `npm start` ou `npm run build` também compila os arquivos Sass.**

## Fazendo deploy no heroku

  1. Faça o login no **heroku**
  
  2. Execute **`git push heroku master`**.
