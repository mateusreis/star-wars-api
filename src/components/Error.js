import React from 'react';
import PropTypes from "prop-types";
import '../css/error.css';

const Error = ({ text }) => (
    <div className="error">
        <p className="error-text">{text}</p>
    </div>
);

Error.propTypes = {
    text: PropTypes.string.isRequired
};
export default Error;