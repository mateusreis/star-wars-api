import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../css/pagination.css';

class Pagination extends Component {

    constructor(props) {
        super(props);
        this.state = {
            next: null,
            previous: null
        };
    }

    componentDidMount() {
        const { next, previous } = this.props;
        this.setState({ next, previous})
    }    

    componentDidUpdate(prevProps) {
        if (this.props.next !== prevProps.next) {
            this.setState({ next: this.props.next });
        }

        if (this.props.previous !== prevProps.previous) {
            this.setState({ previous: this.props.previous });
        }
    }

    gotoPage = url => {
        const { onPageChanged = f => f } = this.props;
        this.setState(() => onPageChanged(url));
    }    

    handlePrevious = evt => {
        evt.preventDefault();
        if (this.state.previous) {
            this.gotoPage(this.state.previous)
        } 
    }

    handleNext = evt => {
        evt.preventDefault();
        if (this.state.next) {
            this.gotoPage(this.state.next)
        } 
    }    

    render(){
        return(
            <div className="pagination">   
                <button className={this.state.previous ? "btn" : "btn disabled"} onClick={this.handlePrevious}>ANTERIOR</button>                     
                <button className={this.state.next ? "btn" : "btn disabled"} onClick={this.handleNext}>PRÓXIMA</button>                     
            </div>
        )
    }
}

Pagination.propTypes = {
    next: PropTypes.string,
    previous: PropTypes.string
};

export default Pagination;