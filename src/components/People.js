import React from 'react';
import PropTypes from "prop-types";
import '../css/people.css';

const People = ({ name }) => (
    <div className="people">
        <h1 className="people-name">{name}</h1>
    </div>
);

People.propTypes = {
    name: PropTypes.string.isRequired
};
export default People;


