import React from 'react';
import PropTypes from "prop-types";
import '../css/header.css';

const Header = ({ title, description }) => (
    <header className="header">
        <h1 className="header-title">{title}</h1>
        <p className="header-text">{description}</p>
    </header>
);

Header.propTypes = {
    title: PropTypes.string.isRequired
};
export default Header;