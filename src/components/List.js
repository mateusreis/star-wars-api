import React, { Component } from "react";
import axios from 'axios';
import Pagination from "./Pagination";
import People from "./People";
import Error from "./Error";
import ajaxLoader from '../images/ajax-loader.gif'; 
import '../css/list.css';

class List extends Component {
    
    state = { 
        people:[],
        count:null,
        next:null,
        previous:null,
        loading:true,
        errorMessage:null
    }

    componentDidMount() {
        this.fetchPeople('https://swapi.co/api/people/')
    }

    fetchPeople(url){
        axios.get(url)
            .then(res => {
                const count = res.data.count;                
                const people = res.data.results;
                const next = res.data.next;
                const previous = res.data.previous;
                this.setState({ people, count, next, previous, loading: false });
            }).catch(error => {
                // console.log(error);
                this.setState({ errorMessage: "Ops! Tivemos um problema, tente novamente mais tarde." });
            });            
    }

    onPageChanged = url => {
        this.fetchPeople(url);
    }

    render() {
        const { people, next, previous, loading, errorMessage } = this.state;
        let data;


        if (loading) {
            data = <div className="loading"><img src={ajaxLoader} alt="carregando" width="31" height="31" /></div>
        } else {
            data = <div>
                    { people.map(people => <People key={people.name} name={people.name}/>) }
                    <Pagination
                        next={next}
                        previous={previous}
                        onPageChanged={this.onPageChanged} />
                    </div>  
        }

        return (
            <div>
                {errorMessage ? <Error text={errorMessage}/> : data }
            </div>
        )
 
    }
}
export default List;
