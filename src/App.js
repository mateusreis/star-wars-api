import React, { Component } from 'react';
import Header from "./components/Header";
import List from "./components/List";

import './css/app.css';

class App extends Component {
  render() {
    return (
      <div className="container">
        <Header title="Star Wars" description="Um guia de personsagens usando a The Star Wars API." />
        <List />
      </div>      
    );
  }
}

export default App;
